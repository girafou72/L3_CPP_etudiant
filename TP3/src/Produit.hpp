// Produit.hpp
#ifndef Produit_HPP_
#define Produit_HPP_

#include <string>

class Produit {
    private:
        std::string _description;
        int _id;
    public:
        Produit(int _id, const std::string & description);
        int getId() const;
        const std::string & getDescription() const;
        void afficherProduit() const;
};

#endif
