//Magasin.cpp
#include "Magasin.hpp"

#include <iostream>

Magasin::Magasin()
{
    _idCourantClient = 0;
    _idCourantProduit = 0;
}
int Magasin::nbClients() const{
   return _clients.size();
}
void Magasin::ajouterClient(const std::string &nom){
    _clients.push_back(Client(_idCourantClient, nom));
    _idCourantClient=_idCourantClient+1;
}
void Magasin::afficherClients()const{
    for(int i=0; i< _clients.size();i++){
        _clients[i].afficherClient();
    }
}
void Magasin::supprimerClients(int idClient){
    for(int i=0; i< _clients.size();i++){
        if(_clients[i].getId() == idClient){
            std::swap(_clients[i],_clients[_clients.size()]);
            _clients.pop_back();
        }
    }
}
