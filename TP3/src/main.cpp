#include <iostream>
#include "Location.hpp"
#include "Client.hpp"
#include "Produit.hpp"
int main(){
    Location palouw;
    palouw._idClient = 0;
    palouw._idProduit = 2;
    palouw.afficherLocation();
    Client pigeon(42,"toto");
    pigeon.afficherClient();
    Produit loot(12,"super pratique avec sa poignée amovible");
    loot.afficherProduit();
	return 0;
}

