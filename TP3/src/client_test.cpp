#include "Client.hpp"
#include <CppUTest/CommandLineTestRunner.h>
#include <string>
TEST_GROUP(GroupClient) { };

TEST(GroupClient, test_client_1) {  // premier test
    Client pierre(123,"test");
    int result = pierre.getId();
    std::string result2 = pierre.getNom();
    CHECK_EQUAL(123, result);
    CHECK_EQUAL("test", result2);
}

