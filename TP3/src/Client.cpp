//Client.cpp
#include "Client.hpp"
#include <iostream>

Client::Client( int id, const std::string & nom) :
		_nom(nom), _id(id)
{}

int Client::getId() const {
		return _id;
}

const std::string & Client::getNom() const {
		return _nom;
}

void Client::afficherClient() const{
		std::cout<< _id << _nom<<std::endl;
}


