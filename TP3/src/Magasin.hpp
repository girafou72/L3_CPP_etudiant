// Magasin.hpp
#ifndef MAGASIN_HPP_
#define MAGASIN_HPP_
#include "Client.hpp"
#include "Produit.hpp"
#include "Location.hpp"
#include <string>
#include <vector>

class Magasin {
    private:
        std::vector <Client> _clients;
        std::vector <Produit> _produits;
        std::vector <Location> _locations;
        int _idCourantClient;
        int _idCourantProduit;
    public:
        Magasin();
        int nbClients() const;
        void ajouterClient(const std::string &nom);
        void afficherClients() const;
		void supprimerClients(int idClient);
};

#endif
