#include "Produit.hpp"
#include <CppUTest/CommandLineTestRunner.h>
#include <string>
TEST_GROUP(GroupProduit) { };

TEST(GroupProduit, test_produit_1) {  // premier test
    Produit pierre(123,"test");
    int result = pierre.getId();
    std::string result2 = pierre.getDescription();
    CHECK_EQUAL(123, result);
    CHECK_EQUAL("test", result2);
}

