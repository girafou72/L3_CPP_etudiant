// Client.hpp
#ifndef CLIENT_HPP_
#define CLIENT_HPP_

#include <string>

class Client {
    private:
        std::string _nom;
        int _id;
    public:
        Client(int _id, const std::string & nom);
        int getId() const;
        const std::string & getNom() const;
        void afficherClient() const;
};

#endif
